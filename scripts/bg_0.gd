extends Node2D
signal destroyed
onready var camera = utils.get_main_node().get_node("camera")
onready var bottom_right = get_node("bottom_right")
func _ready():
	set_process(true)
	pass

func _process(delta):
	if get_global_pos().x + bottom_right.get_global_pos().x <= camera.get_global_pos().x:
		queue_free()
		emit_signal("destroyed")
	pass