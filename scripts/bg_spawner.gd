extends Node2D
const AMOUNT_TO_FILL_VIEW = 3
var bg = preload("res://scenes/bg_0.tscn")
var intersect = 300
var index = 0
var bg_width = 2000
onready var container = find_node("container")
func _ready():
	
	for i in range(AMOUNT_TO_FILL_VIEW):
		spawn_next_bg()
	
	pass
	
func spawn_next_bg():
	spawn_bg()
	go_next_pos()
	pass
	
func spawn_bg():
	var new_bg = bg.instance()

	new_bg.set_pos(get_pos())
	var prev_bg
	if(container.get_child_count() >0):
		prev_bg = container.get_child(get_child_count() - 1)
		index = prev_bg.get_z()
	new_bg.set_z(index-1)
	new_bg.connect("destroyed", self, "spawn_next_bg")
	container.add_child(new_bg)
	pass

func go_next_pos():
	set_pos(get_pos() + Vector2(bg_width - intersect, 0))
	print (str(get_pos().x))
	pass