extends Camera2D

const CAR_HALF = 137 
const MARGIN = 400 + CAR_HALF
const RIGHT_MARGIN = 1600
onready var hero_top = utils.get_main_node().get_node("hero").get_node("top")

func _ready():
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	if ((not utils.is_level_finished) and hero_top.get_global_pos().x > MARGIN and hero_top.get_global_pos().x < utils.GAME_WIDTH - RIGHT_MARGIN): 
		set_pos(Vector2(hero_top.get_pos().x - CAR_HALF, hero_top.get_pos().y - 480))
	elif (not utils.is_level_finished):
		set_pos(Vector2(get_pos().x, hero_top.get_pos().y - 480))
	pass
