extends TextureFrame

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	set_process(true)
	pass
	
func _process(delta):
	var cur_pos = get_global_pos()
	if(cur_pos.x < - 900):
		set_global_pos(Vector2(1920, cur_pos.y))
	else:
		set_global_pos(Vector2(cur_pos.x - 1, cur_pos.y))
	
	pass
