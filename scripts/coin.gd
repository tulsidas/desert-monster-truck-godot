extends Area2D
onready var finished_menu = utils.get_main_node().find_node("finished_menu")
onready var btns_menu = utils.get_main_node().find_node("btns_menu")
onready var score_box = finished_menu.get_node("score_box")
onready var coin_sound = get_node("coin_sound")

const sprite_numbers = [
	preload("res://sprites/number_0.png"),
	preload("res://sprites/number_1.png"),
	preload("res://sprites/number_2.png"),
	preload("res://sprites/number_3.png"),
	preload("res://sprites/number_4.png"),
	preload("res://sprites/number_5.png"),
	preload("res://sprites/number_6.png"),
	preload("res://sprites/number_7.png"),
	preload("res://sprites/number_8.png"),
	preload("res://sprites/number_9.png")
]
func _ready():
	connect("body_enter", self, "_on_body_enter")
	pass

func _on_body_enter(other_body):
	if other_body.is_in_group(utils.GROUP_HERO):
		if(not utils.is_level_finished):
			utils.score +=1
			utils.save(utils.score)
			coin_sound.play()
			update_score()
			btns_menu.hide()
			finished_menu.show()
		utils.is_level_finished = true
		
	pass
	
func update_score():
	var digits = utils.get_digits(utils.score)
	for child in score_box.get_children():
		child.queue_free()
	for digit in digits:
		var texture_frame = TextureFrame.new()
		texture_frame.set_texture(sprite_numbers[digit])
		score_box.add_child(texture_frame)
	pass