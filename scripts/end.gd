extends Area2D

onready var hero = utils.get_main_node().get_node("hero")

func _ready():
	connect("body_enter", self, "_on_body_enter")
	pass

func _on_body_enter(other_body):
	if other_body.is_in_group(utils.GROUP_HERO):
		hero.queue_free()
	pass