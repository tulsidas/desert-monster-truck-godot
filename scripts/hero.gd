extends Node
onready var tire_back = get_node("tire_back")
onready var tire_front = get_node("tire_front")
func _ready():
	get_node("top").add_to_group(utils.GROUP_HERO)
	get_node("snd_drive").play()
	pass
	
func move_forward():
	tire_back.set_angular_velocity(utils.HERO_VELOCITY)
	tire_front.set_angular_velocity(utils.HERO_VELOCITY)
	pass
	
func move_back():
	tire_back.set_angular_velocity(-utils.HERO_VELOCITY)
	tire_front.set_angular_velocity(-utils.HERO_VELOCITY)
	pass
