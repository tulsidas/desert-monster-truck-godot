extends Node


var hero_scene = preload("res://scenes/hero.tscn")
var hero
var hero_top
func _ready():
	create_hero()
	set_process(true)
	pass

func create_hero():
	hero = hero_scene.instance()
	hero.set_pos(Vector2(-380,270))
	hero_top = hero.get_node("top")
	add_child(hero)
	pass
func _process(delta):
	if hero:
		var cur_pos = hero_top.get_global_pos()
		if(cur_pos.x > 2200):
			hero.queue_free()
			create_hero()
		else:
			hero.move_forward()
	pass
