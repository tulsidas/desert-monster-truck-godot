extends ParallaxLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var camera = utils.get_main_node().get_node("camera")
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)
	pass
	
func _process(delta):
	#var cur_pos = get_pos()
	#if(cur_pos.x < -400):
		#set_pos(Vector2(cur_pos.x*(-10), cur_pos.y))
	set_global_pos(Vector2(camera.get_global_pos().x, get_pos().y))
	pass
