extends TextureButton
onready var pause_menu = utils.get_main_node().find_node("pause_menu")
onready var btns_menu = utils.get_main_node().find_node("btns_menu")
func _ready():
	
	pass
	
func _pressed():
	get_tree().set_pause(true)
	btns_menu.hide()
	pause_menu.show()
	pass
