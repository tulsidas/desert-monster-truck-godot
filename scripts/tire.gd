extends RigidBody2D

func _fixed_process(delta):
	
	#set_applied_force(Vector2(get_applied_force().x + 1, get_applied_force().y))
	if(Input.is_action_pressed("ui_right") and not utils.is_level_finished):
		set_angular_velocity(utils.HERO_VELOCITY)
	elif(Input.is_action_pressed("ui_left") and not utils.is_level_finished):
		set_angular_velocity(-utils.HERO_VELOCITY)
	pass
	if(utils.is_level_finished):
		set_angular_velocity(utils.HERO_VELOCITY)
func _ready():
	set_fixed_process(true)
