#script : utils
extends Node

const HERO_VELOCITY = 24
const GAME_WIDTH = 16384
const PLAY = true
var is_level_finished = false
var score = 0
const SPRITES = "res://sprites/"
const GROUP_HERO = "GROUP_HERO"

var savegame = File.new() #file
var save_path = "user://savegame.save" #place of the file
var save_data = {"score": 0} #variable to store data

func create_save():
   savegame.open(save_path, File.WRITE)
   savegame.store_var(save_data)
   savegame.close()

signal score_changed
func _ready():
	if not savegame.file_exists(save_path):
    	create_save()
	else:
		score = read_savegame()
	pass
	
func save(high_score):    
   save_data["score"] = high_score #data to save
   savegame.open(save_path, File.WRITE) #open file to write
   savegame.store_var(save_data) #store the data
   savegame.close() # close the file

func read_savegame():
   savegame.open(save_path, File.READ) #open the file
   save_data = savegame.get_var() #get the value
   savegame.close() #close the file
   return save_data["score"] #return the value
func get_digits(number):
	var digits = []
	if number == 0:
		digits.append(0)
	
	number = abs(number)
	while number > 0:
		var digit = number % 10
		digits.append(digit)
		number = number/10
	
	return digits
	
func get_main_node():
	var root_node = get_tree().get_root()
	return root_node.get_child(root_node.get_child_count() -1)
	pass
