extends Node
var platform_index = 0
var platforms = ["platform0.png","platform1.png","platform2.png","platform3.png", "platform6.png", "platform7.png", "platform8.png"]
var ground_index = 0
var grounds = ["ground1.png","ground2.png","ground3.png","ground4.png", "ground5.png", "ground6.png"]
onready var land = utils.get_main_node().get_node("land")
onready var hero = utils.get_main_node().get_node("hero")
onready var platform = land.get_node("platform")
onready var ground = land.get_node("ground") 
onready var sky = utils.get_main_node().find_node("sky_sprite")
#onready var const_1 = utils.get_main_node().get_node("construct_1") 
#onready var const_3 = utils.get_main_node().get_node("construct_3")
#onready var const_6 = utils.get_main_node().get_node("construct_6")
#onready var const_7 = utils.get_main_node().get_node("construct_7")
const construct_0 = preload("res://scenes/construct_0.tscn")
const construct_1 = preload("res://scenes/construct_1.tscn")
const construct_2 = preload("res://scenes/construct_2.tscn")
const construct_3 = preload("res://scenes/construct_3.tscn")
const construct_4 = preload("res://scenes/construct_4.tscn")
const construct_5 = preload("res://scenes/construct_5.tscn")
const construct_6 = preload("res://scenes/construct_6.tscn")
const construct_7 = preload("res://scenes/construct_7.tscn")
const construct_8 = preload("res://scenes/construct_8.tscn")
const construct_9 = preload("res://scenes/construct_9.tscn")
const construct_10 = preload("res://scenes/construct_10.tscn")
const construct_11 = preload("res://scenes/construct_11.tscn")
const construct_12 = preload("res://scenes/construct_12.tscn")
const construct_13 = preload("res://scenes/construct_13.tscn")
const construct_14 = preload("res://scenes/construct_14.tscn")

const mounds_0 = preload("res://scenes/bg_0.tscn")
const mounds_1 = preload("res://scenes/bg_1.tscn")
const mounds_2 = preload("res://scenes/bg_2.tscn")
const mounds_5 = preload("res://scenes/bg_5.tscn")
const mounds_6 = preload("res://scenes/bg_6.tscn")

const GAP = 700
const VERTICAL_DISTANCE = 536
var horizontal_distance = 1500 
const WORLD_WIDTH = 16384
var obstacle_index = 0
var obstacles = [construct_0, construct_1, construct_2, construct_3, construct_4, construct_5, construct_6, construct_7, construct_8, construct_9, construct_10, construct_11, construct_12, construct_13, construct_14]
var skies = ["sky0.png", "sky1.png", "sky2.png", "sky3.png"]
var mounds = [mounds_0, mounds_1, mounds_2, mounds_5, mounds_6]
var sky_index = 0
var items = 0

func _ready():
	utils.is_level_finished = false
	randomize()
	platform_index = floor(rand_range(0, 6))
	ground_index = floor(rand_range(1,6))
	sky_index = floor(rand_range(0, 3))
	obstacle_index = floor(rand_range(0,14))
	var mounds_index = floor(rand_range(0,4))
	var mounds_layer = find_node("mounds")
	mounds_layer.add_child(mounds[mounds_index].instance())
	
	sky.set_texture(load(utils.SPRITES + skies[sky_index]))
	print ("random" + str(platform_index) + "  " + str(ground_index))
	if platform:
		platform.set_texture(load(utils.SPRITES + platforms[platform_index]))
	if ground:
		ground.set_texture(load(utils.SPRITES + grounds[ground_index]))
	
	while(horizontal_distance < WORLD_WIDTH):
		items+=1
		var construct = obstacles[obstacle_index].instance()
		construct.set_global_pos(Vector2(horizontal_distance, VERTICAL_DISTANCE))
		horizontal_distance = construct.find_node("width").get_global_pos().x + GAP 
		if(horizontal_distance)> WORLD_WIDTH:
			break
		add_child(construct)
		obstacle_index = floor(rand_range(0,14))
		
	pass
	
	
	
	#if const_1:
	#	print("width "+str(const_1.get_node("width").get_global_pos().x))
	